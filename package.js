Package.describe({
  summary: 'aes encryption package',
  version: '1.0.0',
  name: 'jsgoyette:aes',
});

Package.onUse(function (api) {

  api.addFiles('aes.js');

  // Base64 and Sha256 already provided by Meteor
  // api.export('Base64');
  // api.export('Sha256');

  api.export('Utf8');
  api.export('Aes');

});
